Configuration DSCNewRelicAPM
{

    Param(
    [Parameter()]$Computers = (Get-Content "C:\Temp\Computers.txt")


)


    Import-DscResource -ModuleName  PSDesiredStateConfiguration



#region NewRelic APM Agent

	 Node $Computers
	 {

		$NRDotNetAgentPackageLocalPath = "$($env:temp)\NewRelicDotNetAgent_x64.msi"

    		xRemoteFile NRDotNetAgentPackage {
             		Uri = "https://download.newrelic.com/dot_net_agent/latest_release/NewRelicDotNetAgent_x64.msi"
            		DestinationPath = $NRDotNetAgentPackageLocalPath
         	}

         	Package NRDotNetAgent {
             		Ensure = "Present"
             		Path  = $NRDotNetAgentPackageLocalPath
             		Name = "New Relic .NET Agent (64-bit)"
             		ProductId = ""
             		DependsOn = "[xRemoteFile]NRDotNetAgentPackage"
        	}
	 }

#endregion

#region Copy Files from Source to Destination

    Node $Computers

    {

        File copyFolder

        {

            Type = “File”
            Ensure = ‘Present’
            Recurse = $true
            SourcePath = ‘\\adm-a01\CopyFiles\Computers.txt’
            DestinationPath = ‘C:\temp’
            Force = $true



        }

    }


#endregion


}

DSCNewRelicAPM
