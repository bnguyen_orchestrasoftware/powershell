# Usage: pwsh copyFilesRemotely.ps1
# Description: copy a file into a remote windows server.

function createPassword{
  read-host -assecurestring | convertfrom-securestring | out-file bastionCreds
}

function copyFilesRemotely{
# - Copy config.xml file locally to a remote machine
  $File_Origin = "README.md"
  $File_Dest = "README.md"
  $computerName = 'bastion'
  $credsFile = 'bastionCreds'
  Write-Host $File_Origin $File_Dest

  #create the creds object
  $password = get-content $credsFile | convertto-securestring
  $credsObj = new-object -typename Systems.Management.Automation.PSCredential -argumentlist "orchprod\bnguyen-high",$password

  #create the session
  $session = New-PSSession -ComputerName $computerName -Credential $creds
  Write-Host "This is the session: $session"

  #$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection

}

# createPassword
copyFilesRemotely


# --- example ---
# $machines = get-content "PATH GOES HERE"
# $File_Origin = 'PATH GOES HERE'
# $File_Dest = 'PATH GOES HERE'
#
# foreach ($machine in $machines) {
# $session = New-PSSession -ComputerName $machine
# Copy-Item $File_Origin -ToSession $session -Destination $File_Dest -Recurse
# Invoke-command -ComputerName $machine -ScriptBlock {
#    msiexec /qn /i 'PATH GOES HERE' GENERATE_CONFIG=true LICENSE_KEY=19b6e77f1acbba8624f684609eac7bbb8390b73b
# }
#
# Remove-PSSession $session
#
# }
